## Image Classification Using Machine Learning ##

Your objective is to create an object recognition system using machine learning with Gaussian distance classification.

  * Choose 5 different everyday objects.
  * Create software that can differentiate among the objects. You should have at least several examples of each kind of object.
  * Examples:
  
    * Chess pieces
    * Stapler
    * Model cars
    * Model airplanes
    * Fruit
    
  * The primary discrimination between objects should not be color! This makes the problem pretty easy!
  * You may place the objects in front of a single color background for easier identification.
  * You should test your system with more than one example of each object. For example, if you choose to use a stapler as one of your objects, you should test several different size staplers. This will force you to think about what it is that makes a thing a stapler.
  * Report test results for your system using unknown objects. That is, once you think your system is working, test it with never before seen versions of your objects.