# -*- coding: utf-8 -*-

import inspect

import numpy as np
from scipy.signal import convolve2d
import cv2

from os import listdir
from os.path import join
    

def getgrad(img: np.array, kern: np.array) -> np.double:
    grad = convolve2d(img.astype('float'), kern)
    return np.abs(grad).sum()/img.size

class ComponentFuns:
    @staticmethod
    def lenwidratio(img: np.array) -> np.double:
        img = img.astype('bool')
        # For each row
        max_col_len = img.sum(axis=1).max()
        # For each col
        max_row_len = img.sum(axis=0).max()

        return max_row_len / max_col_len

    @staticmethod
    def area_ratio(img: np.array) -> np.double:
        return (img > 0).sum() / img.size

    @staticmethod
    def edge_ratio(img: np.array) -> np.double:
        edges = cv2.Canny(img.astype('uint8'), 100, 200)
        return edges.astype('bool').sum()/img.size

    @staticmethod
    def avg_xgrad_mag(img: np.array) -> np.double:
        kern = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
        return getgrad(img, kern)

    @staticmethod
    def avg_ygrad_mag(img: np.array) -> np.double:
        kern = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]]).T
        return getgrad(img, kern)


def readim_applythresh(imname: str) -> np.array:
    img = cv2.cvtColor(cv2.imread(imname), cv2.COLOR_BGR2GRAY)
    img[img > 195] = 255
    return (255 - img).astype('float', copy=False)


def perform_op(base_im_name, showoutput) -> np.array:
    fnlist = inspect.getmembers(ComponentFuns, predicate=inspect.isfunction)

    base_im_dir = join('./Figs/', base_im_name)
    files = listdir(base_im_dir)
    numfiles = len(files)
    numfuns = len(fnlist)

    imgs = [readim_applythresh(join(base_im_dir, file)) for file in files]

    traindata = np.empty((numfiles, numfuns))
    for ii, file in enumerate(files):
        img = imgs[ii]
        if showoutput:
            print('Image {}:\n'.format(file), end="")
        for jj, (fn_name, componentfun) in enumerate(fnlist):
            fnval = componentfun(img)
            if showoutput:
                print(f'{fn_name:12}: {fnval:2.2f}\n')
            traindata[ii, jj] = fnval
    return traindata


def gen_training_data(showoutput=False):
    return {'write': perform_op('Write', showoutput),
            'calculator': perform_op('Calculator', showoutput),
            'stapler': perform_op('Stapler', showoutput),
            'mug': perform_op('Mug', showoutput),
           }