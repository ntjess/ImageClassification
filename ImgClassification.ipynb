{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Machine Learning: Classifying Images from Parameter Sets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Collection\n",
    "It should seem obvious that you can't classify images without first having images to classify. To drastically simplify the [preprocessing](#Image-Preprocessing) stage, I tried as much as possible to make all image backgrounds uniform and equal to each other. This also included ensuring all images of the same class were in the same orientation and roughly the same size and resolution. During data collection, the simplest way to meet these requirements was to place all objects on a white letter paper background and crop to the page edges. In the end, I collected several subject from the following classes:\n",
    "\n",
    "| **Class**                                      | **Example Image**                                          || **Class**                                |  **Example Image**                                      |\n",
    "| :--------------------------------------------- | :--------------------------------------------------------- || :--------------------------------------- | :------------------------------------------------------ |\n",
    "| Mug                                            | <img src=\"./Figs/Mug/mug7.png\" width=\"100\"/>               || Writing Utensil                          | <img src=\"./Figs/Write/write7.png\" width=\"100\"/>        |\n",
    "| Stapler                                        | <img src=\"./Figs/Stapler/stapler5.png\" width=\"100\"/>       || Other (Objects not in the above classes) | <img src=\"./Figs/Test/otherElephant2.png\" width=\"100\"/> |\n",
    "| Calculator                                     | <img src=\"./Figs/Calculator/calculator9.png\" width=\"100\"/> ||                                          |                                                         |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Image Preprocessing\n",
    "The raw images contain too much information to process in a timely manner, and their backgrounds are too widely varied to produce meaningful outputs. As such, an important stage in the classification algorithm is to first simplify these inputs. This entails simply thesholding the image and inverting it so the subject is white instead of black pixels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "caption": "somecaption",
    "label": "fig:somelabel",
    "pycharm": {
     "is_executing": false
    },
    "widefigure": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import cv2\n",
    "def readim_applythresh(imname: str) -> np.array:\n",
    "    img = cv2.cvtColor(cv2.imread(imname), cv2.COLOR_BGR2GRAY)\n",
    "    img[img > 195] = 255\n",
    "    return (255 - img).astype('float', copy=False)\n",
    "preprocessed_img = readim_applythresh('./Figs/Calculator/calculator9.png')\n",
    "cv2.imwrite('./Figs/ReportFigs/invertedIm.png', preprocessed_img);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<figure style=\"margin-right: auto; margin-left: auto; margin-bottom: 0px; width: 50%\">\n",
    "    <img src=\"./Figs/ReportFigs/invertedIm.png\" style=\"vertical-align: top; margin: 0px;\"/>\n",
    "    <figcaption style=\"text-align: center\">Image after grayscaling, thresholding, and inverting</figcaption>\n",
    "</figure>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Component Selection\n",
    "Once the image is ready to be processed, we must determine a feature set that uniquely identifies elements in one class from another. For instance, we know that a calculator's length-to-width ratio will be much closer to 1 than a writing utensil's, so that may be a good trait to record. Similarly, the eccentricity (measure of foci distances when assuming an eliptical object shape) will be higher for a stapler than a calculator. With this in mind, we can record several traits across all class test images and get a sence of each class' _mean_ and _variance_ for thos traits. I chose to include the following aspects:\n",
    "\n",
    "- Length-to-width ratio: largest sum of all non-black pixels in a given row divided by the largest sum of non-black pixels in a given column\n",
    "- Area ratio: Number of non-black pixels divided by the total number of pixels in the image\n",
    "- Ratio of edge-labeled pixels to total number of pixels, where 'edge pixels' are determined by Canny detection\n",
    "- Average X-gradient: Mean of the absolute sum of changes in the x-direction, as denoted by the formula\n",
    "  \\begin{equation}\n",
    "  \\dfrac{1}{\\text{numel($I$)}}\\sum_{i=1}^{\\text{numel($I$)}}\\left|\\nabla^x_i(I)\\right|, \\ I=\\text{ image with pixels $i$}.\n",
    "  \\end{equation}\n",
    "  \n",
    "    Since this quantity can vary widely among each image, it is finally scaled so its output magnitude matches that of all other parameters.\n",
    "- Average Y-gradient: Similar to the previous parameter, but in the y-direction:\n",
    "  \\begin{equation}\n",
    "  \\dfrac{1}{\\text{numel($I$)}}\\sum_{i=1}^{\\text{numel($I$)}}\\left|\\nabla^y_i(I)\\right|\n",
    "  \\end{equation}\n",
    "  As with the X-gradient average, this quantity too is scaled."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training Data\n",
    "Using the previously described parameter list, we can form a matrix of data for each class with one column for each classification parameter and one row for every training sample:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "pycharm": {
     "is_executing": false
    }
   },
   "outputs": [],
   "source": [
    "import pickle\n",
    "from Code.gen_training_data import gen_training_data\n",
    "\n",
    "########## DATA GENERATION ##########\n",
    "data = gen_training_data(showoutput=False)\n",
    "group_means = [comp.mean(axis=0) for comp in data.values()]\n",
    "\n",
    "f = open('./Code/train.pkl', 'wb')\n",
    "pickle.dump(data, f)\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For instance, after running the above code the 'calculator' class would record the following values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "hide_input": true,
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<b>Collected data from the \"calculator\" class:</b>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>area_ratio</th>\n",
       "      <th>avg_xgrad_mag</th>\n",
       "      <th>avg_ygrad_mag</th>\n",
       "      <th>edge_ratio</th>\n",
       "      <th>lenwidratio</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.279088</td>\n",
       "      <td>14.178269</td>\n",
       "      <td>13.340825</td>\n",
       "      <td>0.025341</td>\n",
       "      <td>2.217391</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.309934</td>\n",
       "      <td>12.733515</td>\n",
       "      <td>12.065592</td>\n",
       "      <td>0.022371</td>\n",
       "      <td>2.235679</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.247216</td>\n",
       "      <td>10.415932</td>\n",
       "      <td>9.865425</td>\n",
       "      <td>0.015243</td>\n",
       "      <td>2.175000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>0.195213</td>\n",
       "      <td>7.261427</td>\n",
       "      <td>8.430567</td>\n",
       "      <td>0.013763</td>\n",
       "      <td>0.655405</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>0.128117</td>\n",
       "      <td>6.677185</td>\n",
       "      <td>8.125030</td>\n",
       "      <td>0.011996</td>\n",
       "      <td>1.159055</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>0.248434</td>\n",
       "      <td>13.584733</td>\n",
       "      <td>12.452624</td>\n",
       "      <td>0.021942</td>\n",
       "      <td>2.144462</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>0.191876</td>\n",
       "      <td>9.326192</td>\n",
       "      <td>8.759358</td>\n",
       "      <td>0.014126</td>\n",
       "      <td>1.896797</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>0.088423</td>\n",
       "      <td>5.754902</td>\n",
       "      <td>5.593795</td>\n",
       "      <td>0.010713</td>\n",
       "      <td>1.605898</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>0.311803</td>\n",
       "      <td>10.521975</td>\n",
       "      <td>8.470295</td>\n",
       "      <td>0.011633</td>\n",
       "      <td>2.139860</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>0.192529</td>\n",
       "      <td>7.822561</td>\n",
       "      <td>6.959110</td>\n",
       "      <td>0.009598</td>\n",
       "      <td>1.817647</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>0.271589</td>\n",
       "      <td>12.172393</td>\n",
       "      <td>10.713303</td>\n",
       "      <td>0.019475</td>\n",
       "      <td>2.220963</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "    area_ratio  avg_xgrad_mag  avg_ygrad_mag  edge_ratio  lenwidratio\n",
       "0     0.279088      14.178269      13.340825    0.025341     2.217391\n",
       "1     0.309934      12.733515      12.065592    0.022371     2.235679\n",
       "2     0.247216      10.415932       9.865425    0.015243     2.175000\n",
       "3     0.195213       7.261427       8.430567    0.013763     0.655405\n",
       "4     0.128117       6.677185       8.125030    0.011996     1.159055\n",
       "5     0.248434      13.584733      12.452624    0.021942     2.144462\n",
       "6     0.191876       9.326192       8.759358    0.014126     1.896797\n",
       "7     0.088423       5.754902       5.593795    0.010713     1.605898\n",
       "8     0.311803      10.521975       8.470295    0.011633     2.139860\n",
       "9     0.192529       7.822561       6.959110    0.009598     1.817647\n",
       "10    0.271589      12.172393      10.713303    0.019475     2.220963"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import pandas as pd\n",
    "from IPython.display import display, HTML\n",
    "from Code.gen_training_data import ComponentFuns\n",
    "import inspect\n",
    "\n",
    "np.set_printoptions(precision=2,suppress=True)\n",
    "sample_class = 'calculator'\n",
    "col_label_tuple = inspect.getmembers(ComponentFuns, predicate=inspect.isfunction)\n",
    "col_labels = [name for name,_ in col_label_tuple]\n",
    "frame = pd.DataFrame(data[sample_class], columns=col_labels)\n",
    "display(HTML(f'<b>Collected data from the \"{sample_class}\" class:</b>'))\n",
    "display(frame)\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Decision Metric\n",
    "This portion of our classification algorithm would be considered the 'meat and potatoes' of the process, as it is what generates the final number designating a class for the input image.\n",
    "\n",
    "We start by obtaining covariance and mean matrices from the training data for each class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "def cov_mat(traindata: np.array) -> (np.array, np.array):\n",
    "    M = traindata.mean(0)\n",
    "    # Subtract mean from each column before determining covariance\n",
    "    traindata -= M\n",
    "    # Transpose before covariance since we are dealing with row vectors\n",
    "    C = np.cov(traindata.T)\n",
    "    return (C, M)\n",
    "\n",
    "# Preallocate C and M matrices for each class\n",
    "num_classes = len(data)\n",
    "# Index arbitrary class data matrix, since all column numbers must be equal\n",
    "num_components = next(iter(data.values())).shape[1]\n",
    "C = np.empty((num_components, num_components, num_classes))\n",
    "# Stay consistent so 3rd dimension represents class in both matrices\n",
    "M = np.empty((1, num_components, num_classes))\n",
    "\n",
    "for idx, (_, objvals) in enumerate(data.items()):\n",
    "    tmp_c, tmp_m = cov_mat(objvals)\n",
    "    C[:,:,idx] = tmp_c\n",
    "    M[0,:,idx] = tmp_m\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once **$C$** and **$M$** values are obtained for each known class, we create matrix **$X$** by evaluating the component functions for the incoming image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Batch process for efficiency\n",
    "from os import listdir\n",
    "from os.path import join\n",
    "from Code.gen_training_data import perform_op\n",
    "\n",
    "X = perform_op('Test', False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**$X$**, **$C$**, and **$M$** give us all the necessary pieces to implement gaussian-distribution classification via the formula\n",
    "\\begin{equation}\n",
    "d_j(\\mathbf{X}) = -\\dfrac{1}{2}\\ln|\\mathbf{C}| - \\dfrac{1}{2}\\left(\\mathbf{X}-\\mathbf{M}\\right)\\cdot\\mathbf{C^{-1}}\\cdot\\left(\\mathbf{X}-\\mathbf{M}\\right)\n",
    "\\end{equation}\n",
    "Where $\\mathbf{X}$ belong to class $j$ if $d_j(\\mathbf{X})$ is the largest value for $j=1...W$ for $W$ classes. \n",
    "\n",
    "In our case, we also want to determine if an input image did not belong to _any_ known class. To implement this feature, we simply enacted a threshold such that if $d_j$ was too low (i.e. we have a very low certainty of the class in which this image belongs), $\\mathbf{X}$ was assigned to class 'other'.\n",
    "\n",
    "In this manner, we are able to classify a host of incoming images through comparing them to a sample set of known class values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy.linalg import det, inv\n",
    "def per_class_metric(X: np.array, C: np.array, M: np.array) -> np.array:\n",
    "    # Vectorize processing: Create new dimension for each class comparison\n",
    "    X[None,:] -= M\n",
    "    return -0.5*np.log(np.abs(det(C))) - 0.5*(X)@(inv(C)@X.T)\n",
    "\n",
    "certainties = np.empty((X.shape[0], num_classes))\n",
    "for class_idx, (_, compdata) in enumerate(data.items()):\n",
    "    certainties[:,class_idx] = np.apply_along_axis(\\\n",
    "         per_class_metric, 1, X, C[...,class_idx], M[...,class_idx])\n",
    "# Now each X row has a confidence level per class. Indicate X belongs to the\n",
    "# class with the highest certainty, UNLESS that certainty is too low. In that\n",
    "# case, assign X to class 'other'\n",
    "x_clsfcn_idxs = np.argmax(certainties, 1)\n",
    "x_clsfcn_idxs[certainties[np.arange(X.shape[0]),x_clsfcn_idxs] < -1000] = num_classes\n",
    "class_list = np.array(list(data.keys()))\n",
    "class_list = np.append(class_list, 'other')\n",
    "x_class_names = class_list[x_clsfcn_idxs]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Output\n",
    "And there you have it! x_class_names holds the determined class of each image in the `Test` folder. Many aspects of this algorithm could be improved, such as the component functions, decision metric, etc. but all the basics of image classification are present.\n",
    "\n",
    "Furthermore, sample image quality could be drastically improved. Data collection occurred in a variety of different lightings and settings, so it was inevitable that some shadows persisted through the thresholding process and corrupted some measurements. Nonetheless, the classification program successfully identified almost half the incoming images.\n",
    "\n",
    "It is also worth noting that altering the threshold for when a class is considered 'other' would also change the output result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<table><tr style=\"background-color:white;\"><td style=\"vertical-align:top\"><span style=\"width:50%;margin-right:25%\"><b>Correctly Classified</b></span><div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Image Name</th>\n",
       "      <th>Image Classification</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>calculator1.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>calculator2.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>calculator3.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>calculator4.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>calculator5.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>calculator6.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>stapler8.png</td>\n",
       "      <td>stapler</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>stapler9.png</td>\n",
       "      <td>stapler</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>write1.png</td>\n",
       "      <td>write</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>write2.png</td>\n",
       "      <td>write</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>write3.png</td>\n",
       "      <td>write</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>write4.png</td>\n",
       "      <td>write</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>12</th>\n",
       "      <td>write5.png</td>\n",
       "      <td>write</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>13</th>\n",
       "      <td>write6.png</td>\n",
       "      <td>write</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div></td><td style=\"vertical-align:top\"><span style=\"width:50%;margin-right:25%\"><b>Wrongly Classified</b></span><div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Image Name</th>\n",
       "      <th>Image Classification</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>mug1.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>mug2.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>mug3.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>mug4.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>mug5.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>mug6.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>otherAllWireCutters.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>otherElephant1.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>otherElephant2.png</td>\n",
       "      <td>write</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>otherElephant3.png</td>\n",
       "      <td>write</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>otherGlasses1.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>otherKeys1.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>12</th>\n",
       "      <td>otherPaper1.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>13</th>\n",
       "      <td>otherWireCutter1.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>14</th>\n",
       "      <td>otherWireCutter2.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>15</th>\n",
       "      <td>otherWireCutter3.png</td>\n",
       "      <td>stapler</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>16</th>\n",
       "      <td>otherWireCutter4.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17</th>\n",
       "      <td>stapler1.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>18</th>\n",
       "      <td>stapler2.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>19</th>\n",
       "      <td>stapler3.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>20</th>\n",
       "      <td>stapler6.png</td>\n",
       "      <td>calculator</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div></td></tr></table>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "im_names = listdir('./Figs/Test')\n",
    "iscorrect = [x_class_names[ii] in im_names[ii]\\\n",
    "                  for ii, _ in enumerate(im_names)]\n",
    "is_incorrect = [not correct for correct in iscorrect]\n",
    "\n",
    "tbl_data = np.fliplr(np.array([x_class_names, im_names]).T)\n",
    "crct_tbl = tbl_data[iscorrect,:]\n",
    "incrct_tbl = tbl_data[is_incorrect,:]\n",
    "\n",
    "col_names = columns=['Image Name', 'Image Classification']\n",
    "crctframe = pd.DataFrame(crct_tbl, columns=col_names)\n",
    "wrongframe = pd.DataFrame(incrct_tbl, columns=col_names)\n",
    "\n",
    "def multi_table(table_list, title_list):\n",
    "    ''' Acceps a list of IpyTable objects and returns a table which contains each IpyTable in a cell\n",
    "    '''\n",
    "    return HTML(\n",
    "        '<table><tr style=\"background-color:white;\">' + \n",
    "        ''.join(['<td style=\"vertical-align:top\">' +\\\n",
    "                 f'<span style=\"width:50%;margin-right:25%\"><b>{title}</b></span>' + table._repr_html_() +\\\n",
    "                 '</td>' for table, title in zip(table_list, title_list)]) +\n",
    "        '</tr></table>'\n",
    "    )\n",
    "\n",
    "multi_table([crctframe, wrongframe], ['Correctly Classified', 'Wrongly Classified'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "pycharm": {
   "stem_cell": {
    "cell_type": "raw",
    "metadata": {
     "collapsed": false
    },
    "source": []
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
